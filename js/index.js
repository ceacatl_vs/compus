var btnContacto;
    
function getBtnId(clicked_id) {
	btnContacto = clicked_id;
};

$(function () {
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();
	$('.carousel').carousel({
    	interval: 2000
    });

    $('#mdlContacto').on('show.bs.modal', function (e) {
    	console.log('El modal contacto se está mostrando');
    	console.log('Se uso el botón: ' + btnContacto);

    	$('#' + btnContacto).removeClass('btn-success');
    	$('#' + btnContacto).addClass('btn-outline-success');
    	$('#' + btnContacto).prop('disabled', true);
    });    
    $('#mdlContacto').on('shown.bs.modal', function (e) {
		console.log('El modal contacto se mostró');
	});
	$('#mdlContacto').on('hide.bs.modal', function (e) {
		console.log('El modal contacto se oculta');
	});
	$('#mdlContacto').on('hidden.bs.modal', function (e) {
		console.log('El modal contacto se ocultó');

		$('#' + btnContacto).removeClass('btn-outline-success');
    	$('#' + btnContacto).addClass('btn-success');
    	$('#' + btnContacto).prop('disabled', false);
	});
});
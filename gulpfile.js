'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var del = require('del');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var usemin = require('gulp-usemin');
var rev = require('gulp-rev');
var cleanCss = require('gulp-clean-css');
var flatmap = require('gulp-flatmap');
var htmlmin = require('gulp-htmlmin');

gulp.task('sass', function() {
	return gulp.src('./css/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function() {
	gulp.watch('./css/*.scss', gulp.series('sass'));
});

gulp.task('browser-sync', function() {
	var files = ['./*.html', './css/*.css', './img/*.{png, jpg, jpeg, gif}', './js/*.js']
	browserSync.init(files, {
		server: {
			baseDir: "./"
		}
	});
});

gulp.task('clean', function() {
	return del(['dist']);
});

gulp.task('copyfonts', function(){
	return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf, woff, eof, svg, eot, otf}*')
	.pipe(gulp.dest('dist/fonts'));
});

gulp.task('imagemin', function() {
	return gulp.src('./images/*')
		.pipe(imagemin([
			imagemin.gifsicle({interlaced: true}),
			imagemin.mozjpeg({quality: 75, progressive: true}),
			imagemin.optipng({optimizationLevel: 3})
		]))
		.pipe(gulp.dest('dist/images'));
});

gulp.task('usemin', function() {
	return gulp.src('./*.html')
		.pipe(flatmap(function(stream, file) {
			return stream
				.pipe(usemin({
					css: [rev()],
					html: [htmlmin({collapseWhitespace: true })],
					js: [uglify(), rev()],
					inlinejs: [uglify()],
					inlinecss: [cleanCss(), 'concat']
				}))			
		}))
		.pipe(gulp.dest('dist/'));
});

gulp.task('default', gulp.parallel('browser-sync', 'sass:watch'));
gulp.task('build', gulp.series('clean', 'copyfonts', 'imagemin', 'usemin'));